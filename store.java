//https://code.google.com/codejam/contest/351101/dashboard#s=p0
public class store {
    public static void main(String[] args) {
        int credit = 100;
        int size = 3;
        int[] itemPrices = new int[]{5, 75, 25};

        String answer;
        for(int i=0; i<size-1; i++) {
            for(int j=i+1; j<size; j++) {
                if(itemPrices[i]+itemPrices[j] == credit) {
                    answer = (i + 1) + " " + (j + 1);
                    System.out.println("answer = " + answer);
                }
            }
        }

    }
}
