//https://code.google.com/codejam/contest/351101/dashboard#s=p1
import java.util.ArrayList;

public class reverseWords {
    public static void main(String[] args) {
        String input = "this is a test";
        ArrayList<String> words = new ArrayList<>();
        int prev = 0;
        for(int i =0; i< input.length(); i++) {
            if(input.charAt(i) == ' ') {
                String temp = input.substring(prev, i);
                words.add(temp);
                prev = i;
            }
        }
        String temp = input.substring(prev, input.length());
        words.add(temp);
        for(int i=words.size()-1; i>=0; i--) {
            System.out.println(words.get(i));
        }
    }
}
